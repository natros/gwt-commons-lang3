/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package java.lang;

import java.util.WeakHashMap;

public class ThreadLocal<T> {
	
    private WeakHashMap<Object, Object> map;

    /**
     * Creates a thread local variable.
     */
    public ThreadLocal() {
    	this.map = new WeakHashMap<Object, Object>();
    }

    @SuppressWarnings("unchecked")
	public T get() {
    	return (T) map;
    }

    /**
     * Sets the current thread's copy of this thread-local variable
     * to the specified value.  Most subclasses will have no need to
     * override this method, relying solely on the {@link #initialValue}
     * method to set the values of thread-locals.
     *
     * @param value the value to be stored in the current thread's copy of
     *        this thread-local.
     */
    @SuppressWarnings("unchecked")
	public void set(T value) {
    	this.map = (WeakHashMap<Object, Object>) value;
    }

     public void remove() {
    	 // nothing to do.
     }
}
